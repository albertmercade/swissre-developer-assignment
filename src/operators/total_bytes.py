from operators.log_operator import LogOperator
from model.log_record import LogRecord

"""ASSUMPTIONS
    - The total amount of bytes is the sum of the header and response bytes.
"""

class TotalBytesExchangedOperator(LogOperator):
    """Implementation of 'Total amount of bytes exchanged' operator"""

    op_name = "Total amount of bytes exchanged"
    op_description = "Compute the total amount of bytes exchanged"

    def __init__(self):
        self._total_bytes = 0

    def update_operator(self, log: LogRecord):
        self._total_bytes += log.get_total_bytes()

    def get_result(self):
        # Return total bytes exchanged
        return self._total_bytes