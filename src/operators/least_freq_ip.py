import functools

from operators.log_operator import LogOperator
from model.log_record import LogRecord

"""ASSUMPTIONS
    - If more than one IP are equally the least frequent,
    only one of them is provided as the result.
"""

class LeastFrequentIPOperator(LogOperator):
    """Implementation of 'Least Frequent IP' operator"""

    op_name = "Least frequent IP"
    op_description = "Compute least frequent IP"

    def __init__(self):
        self._freq = {}

    def update_operator(self, log: LogRecord):
        ip = log.client_ip

        # If this IP isn't in the frequency map, add it
        if ip not in self._freq:
            self._freq[ip] = 0
        
        self._freq[ip] += 1

    def get_result(self):
        # Return the least frequent IP
        if len(self._freq) == 0:
            return None
        return min(self._freq, key=self._freq.get)