FROM python:3.7-slim

WORKDIR /app

COPY . .

ENTRYPOINT [ "python", "src/main.py" ]