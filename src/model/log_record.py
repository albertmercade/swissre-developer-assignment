from datetime import datetime

"""ASSUMPTIONS
    - While the input file type may change, each log record will always
    contain the fields outlined in the assignment and in the same format.
"""

class LogRecord:
    """Class representing a log record"""
    def __init__(self, timestamp: str, resp_header_size: str, client_ip: str,
            http_resp_code: str,resp_size: str, http_req_method: str, url: str,
            username: str, access_type: str, dest_ip: str, resp_type: str):

        self.timestamp = datetime.fromtimestamp(float(timestamp))
        self.resp_header_size = int(resp_header_size)
        self.client_ip = client_ip
        self.http_resp_code = http_resp_code
        self.resp_size = int(resp_size)
        self.http_req_method = http_req_method
        self.url = url
        self.username = username
        self.access_type = access_type
        self.dest_ip = dest_ip
        self.resp_type = resp_type

    def get_total_bytes(self):
        """Return total bytes exchanged in this event"""
        return self.resp_header_size + self.resp_size

