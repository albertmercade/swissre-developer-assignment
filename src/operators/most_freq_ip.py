from operators.log_operator import LogOperator
from model.log_record import LogRecord

"""ASSUMPTIONS
    - If more than one IP are equally the most frequent,
    only one of them is provided as the result.
"""

class MostFrequentIPOperator(LogOperator):
    """Implementation of 'Most Frequent IP' operator"""
    
    op_name = "Most frequent IP"
    op_description = "Compute most frequent IP"

    def __init__(self):
        self._freq = {}
        self._most_freq = None

    def update_operator(self, log: LogRecord):
        ip = log.client_ip

        # If this IP isn't in the frequency map, add it
        if ip not in self._freq:
            self._freq[ip] = 0
        self._freq[ip] += 1

        # If the IP is more frequent than he current most frequent IP,
        # update it
        if self._most_freq is None or self._freq[self._most_freq] < self._freq[ip]:
            self._most_freq = ip

    def get_result(self):
        # Return the most frequent IP
        return self._most_freq