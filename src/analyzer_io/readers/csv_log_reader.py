import csv
from io import TextIOWrapper

from analyzer_io.readers.log_reader import LogReader
from model.log_record import LogRecord


"""ASSUMPTIONS
    - Since the first line of the sample input file was blank, I assumed it
    will always be like that and therefore skip it.
"""

class CSVLogReader(LogReader):
    """LogReader implementation to read CSV log files"""

    def __init__(self, file_obj: TextIOWrapper):
        self._reader = csv.reader(file_obj, delimiter=" ", skipinitialspace=True)
        # Skip first empty line
        next(self._reader, None)

    def read_next_log_record(self):
        # Read next log record from file
        fields = next(self._reader, None)

        # If EOF reached, return None
        if fields is None:
            return None

        # Instantiate and return a LogRecord 
        access_type, dest_ip = fields[8].split("/")
        return LogRecord(
            timestamp=fields[0],
            resp_header_size=fields[1],
            client_ip=fields[2],
            http_resp_code=fields[3],
            resp_size=fields[4],
            http_req_method=fields[5],
            url=fields[6],
            username=fields[7],
            access_type=access_type,
            dest_ip=dest_ip,
            resp_type=fields[9]
        )