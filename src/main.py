from log_analyzer import LogAnalyzer

"""Information about how to build and run docker image as well as
    about how the application could be extended in README.
"""

def main():
    # Initialize LogAnalyzer instance
    analyzer = LogAnalyzer()

    # Read and analyze log records
    analyzer.analyze_logs()

    # Write operation results to output file
    analyzer.write_output()

if __name__ == "__main__":
    main()