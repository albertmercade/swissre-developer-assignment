from typing import List

from operators.most_freq_ip import MostFrequentIPOperator
from operators.least_freq_ip import LeastFrequentIPOperator
from operators.total_bytes import TotalBytesExchangedOperator
from operators.events_per_sec import EventsPerSecondOperator

OPERATORS = {
    "op_most_freq_ip": MostFrequentIPOperator,
    "op_least_freq_ip": LeastFrequentIPOperator,
    "op_total_bytes": TotalBytesExchangedOperator,
    "op_events_per_sec": EventsPerSecondOperator
}

def get_operators(needed_ops: List[str]):
    """Returns operator implementations according to selected operations"""
    return [OPERATORS[op]() for op in needed_ops]