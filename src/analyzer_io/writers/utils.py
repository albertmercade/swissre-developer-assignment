from io import TextIOWrapper

from analyzer_io.writers.json_output_writer import JSONOutputWriter

WRITERS = {
    "json": JSONOutputWriter
}

def get_output_writer(file_type: str, file_obj: TextIOWrapper):
    """Return writer instance according to output file type"""
    return WRITERS[file_type](file_obj)

def get_output_writer_extension(file_type: str):
    """Return output file extension according to output file type"""
    return WRITERS[file_type].file_extension