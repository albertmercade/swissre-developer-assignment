from pathlib import Path
import sys

from analyzer_io import InputParser
from analyzer_io.readers import get_log_reader
from analyzer_io.writers import get_output_writer, get_output_writer_extension
from operators import get_operators

"""ASSUMPTIONS
    - If all input files are empty, the output will still be written to a file
    with default values assigned (eg. 0, null)
    - The output file will be stored in the directory where the app is run.
"""

class LogAnalyzer:
    """Implementation of the main logic of the tool"""

    def __init__(self):
        parser = InputParser()
        parser.parse_input()

        self._file_paths = parser.file_paths
        self._valid_input_paths = len(parser.file_paths) > 0
        self._operators = get_operators(parser.operations)

        self._input_type = parser.input_type
        self._output_type = parser.output_type

    def analyze_logs(self):
        """Analyze the logs for every input file"""

        if not self._valid_input_paths:
            print("ERROR: No valid input paths. Finishing execution without output.")
            sys.exit(1)

        for file in self._file_paths:
            self._analyze_file(file)
    
    def _analyze_file(self, file: Path):
        """Read file contents and for every log record apply desired operations."""

        try:
            log_file = open(file)
        except OSError:
            print(f"ERROR: Couldn't open file: {file}")
            print("\tThe file's content will be ignored.")
        else:
            with log_file:
                # Get reader instance according to input file type
                reader = get_log_reader(self._input_type, log_file)
                # Loop through log records in file and apply operations
                log = reader.read_next_log_record()
                while log:
                    for op in self._operators:
                        op.update_operator(log)
                    log = reader.read_next_log_record()
        finally:
            log_file.close()

    def write_output(self):
        """Write operation results to output file and print output file path"""

        # Get operation results
        output = self._format_output([op.get_output() for op in self._operators])

        # Get path to output file
        curr_work_dir = Path().resolve()
        file_ext = get_output_writer_extension(self._output_type)

        file = curr_work_dir / f"output.{file_ext}"

        try:
            output_file = open(file, 'w')
        except OSError:
            print(f"ERROR: Couldn't create output file.")
        else:
            with output_file:
                # Get writer instance according to output file type
                writer = get_output_writer(self._output_type, output_file)
                # Write to output to output file
                writer.write_output(output)
            
            print(f"Path to output file: {file}")
        finally:
            output_file.close()
    
    def _format_output(self, output: dict):
        """Format output dictionary of operation"""
        
        return {op_output["name"]: op_output["result"] for op_output in output}