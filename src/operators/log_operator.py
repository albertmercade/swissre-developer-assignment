from abc import ABC, abstractmethod
from model.log_record import LogRecord

class LogOperator(ABC):
    """Base class for implementing log operators"""

    # Abstract class properties
    @property
    @abstractmethod
    def op_name(self):
        """Name of the operator"""
        pass

    @property
    @abstractmethod
    def op_description(self):
        """Description of the operator"""
        pass

    # Abstract methods
    @abstractmethod
    def update_operator(self, log: LogRecord):
        """Update operation calculation with new LogRecord"""
        pass

    @abstractmethod
    def get_result(self):
        """Return result of operation"""
        pass

    # Non-abstract methods
    def get_output(self):
        """Return name and result of operation"""
        return {
            "name": self.op_name,
            "result": self.get_result()
        }

    def __str__(self):
        return f"{self.op_name}: {self.get_result()}"