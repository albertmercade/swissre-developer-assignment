from abc import ABC, abstractmethod

class LogReader(ABC):
    """Base class for implementing log file readers"""
    
    # Abstract methods
    @abstractmethod
    def read_next_log_record(self):
        """Return a LogRecord instance of the next log record
        read from input file
        """
        pass