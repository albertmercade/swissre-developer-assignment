from abc import ABC, abstractmethod

class OutputWriter(ABC):
    """Base class for implementing output writers"""
    
    # Abstract property
    @property
    @abstractmethod
    def file_extension(self):
        """Extension of the file type written by the implementation"""
        pass

    # Abstract method
    @abstractmethod
    def write_output(self, output: dict):
        """Write received output to output file"""
        pass