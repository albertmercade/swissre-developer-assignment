# Swiss Re - Developer Assignment

Coding assignment for the Junior Software Engineer role at Swiss Re.

## Build

Run from repository root:

```shell
docker build -t <TAG> .
```

## Run

To learn about the options and arguments accepted:

```shell
docker run <TAG> -h
```

To run the tool:

```shell
docker run -v ${PWD}:/app <TAG> [OPTIONS] <PATH_TO_INPUT_FILE>
```

The output file will be copied to your working directory.

## Extending application

### Adding input log formats

To add a new input log format simply implement it as a subclass of `LogReader`
(found in `src/analyzer_io/readers/log_reader.py`) and add it to the
`READERS` dictionary in `src/analyzer_io/readers/utils.py`.
Use `CSVLogReader` as an example.

The key used in the dictionary to identify the input format will be the one
of the accepted choices as argument for the `--input-type` option.

### Adding output formats

Similarly to adding a new input format, to add a new output format simply
implement it as a subclass of `OutputWriter`
(found in `src/analyzer_io/writers/output_writer.py`) and add it to the
`WRITERS` dictionary in `src/analyzer_io/writers/utils.py`.
Use `JSONOutputWriter` as an example.

The key used in the dictionary to identify the output format will be the one
of the accepted choices as argument for the `--output-type` option.

### Adding operations

To add new operations, implement them as subclasses of `LogOperator`
(found in `src/operators/log_operator.py`) and add it to the `OPERATORS`
dictionary in `src/operators/utils.py`. Use any of the existing operators
as an example, for instance `MostFrequentIPOperator`.

The key used in the dictionary to identify the operation will be the
flag used as argument to select that operation, replacing the underscores
with hyphens. The key must start with `op-` and the words must be separated by underscores. For example, if we have
`OPERATORS = {"op_most_freq_ip": MostFrequentIPOperator}` then the flag to select that operation will be `--op-most-freq-ip`.
