import json
from io import TextIOWrapper

from analyzer_io.writers.output_writer import OutputWriter

class JSONOutputWriter(OutputWriter):
    """JSON output writer implementation"""
    
    file_extension = "json"

    def __init__(self, file_obj: TextIOWrapper):
        self._writer = file_obj

    def write_output(self, output: dict):
        json_output = json.dumps(output, indent=4)
        self._writer.write(json_output)