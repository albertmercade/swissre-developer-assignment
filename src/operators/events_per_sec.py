from datetime import datetime

from operators.log_operator import LogOperator
from model.log_record import LogRecord

"""ASSUMPTIONS
    - The events per second operation is calculated as the total amount of 
    events divided by the seconds that occur between the earliest event and
    the most recent.
"""

class EventsPerSecondOperator(LogOperator):
    """Implementation of 'Events per second' operator"""
    op_name = "Events per second"
    op_description = "Compute the number of events per second"

    def __init__(self):
        self._min_time = datetime.max
        self._max_time = datetime.min
        self._event_count = 0

    def update_operator(self, log: LogRecord):
        self._min_time = min(self._min_time, log.timestamp)
        self._max_time = max(self._max_time, log.timestamp)
        self._event_count += 1

    def get_result(self):
        # Return events per second
        if self._event_count == 0:
            return 0
        
        return self._event_count / (self._max_time - self._min_time).total_seconds()