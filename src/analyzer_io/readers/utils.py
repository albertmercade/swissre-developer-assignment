from io import TextIOWrapper

from analyzer_io.readers.csv_log_reader import CSVLogReader

READERS = {
    "csv": CSVLogReader
}

def get_log_reader(file_type: str, file_obj: TextIOWrapper):
    """Return reader instance according to input file type"""
    return READERS[file_type](file_obj)