from argparse import ArgumentParser
from pathlib import Path
from typing import List

from operators import OPERATORS
from analyzer_io.readers import READERS
from analyzer_io.writers import WRITERS

"""ASSUMPTIONS
    - If no operation flag is provided, it is assumed that all operations
    should be performed.
    - Any file in an input directory is a valid log file.
    - If a directory is provided as input, the -r (or --recursive) flag
    can be provided to indicate that directories should be recursively 
    inspected looking for log files. Otherwise, the default behaviour is
    to only consider files directly inside the directory.
    - If no input file type is indicated, CSV is assumed.
    - If not output file is indicated, JSON is assumed.
"""

class InputParser():
    def __init__(self):
        self.setup_args_parser()

    def setup_args_parser(self):
        """Instantiate, setup and return an ArgumentParser to parse the given
        command-line arguments and check for errors.
        """

        # Create parser 
        self._parser = ArgumentParser(
            description="A command line tool to analyze the content of log files.",
            epilog="If no operation is selected, all operations are computed.",
            usage="%(prog)s [options] path [path ...]"
        )

        # Options
        ## Input file type
        self._parser.add_argument(
            "--input-type",
            default="csv",
            choices=READERS.keys(),
            help="Input file type to expect (default: csv)"
        )

        ## Flag to recursively analyze input directories
        self._parser.add_argument(
            "-r", "--recursive",
            action="store_true",
            help="Recursively analyze input directories"
        )

        ## Output file type
        self._parser.add_argument(
            "--output-type",
            default="json",
            choices=WRITERS.keys(),
            help="Output file type to expect (default: json)"
        )

        ## Flags to indicate which operations should be computed
        for op_id, op_class in OPERATORS.items():
            flag_name = f"--{op_id.replace('_','-')}"
            flag_help = op_class.op_description
            self._parser.add_argument(
                flag_name,
                action="store_true",
                help=flag_help
            )

        # File/directory path argument
        self._parser.add_argument(
            "path",
            nargs="+",
            type=Path,
            help="Path to a plain text file or directory"
        )

    def parse_input(self):
        """Parse and format arguments"""

        args = self._parser.parse_args()

        # If no operation is selected, select all of them
        all_operations = {k: v for k,v in vars(args).items() if k.startswith("op_")}
        all_false = not any(all_operations.values())

        self._recursive = args.recursive

        self.file_paths = self.input_paths_resolution(args.path)
        self.input_type = args.input_type
        self.output_type = args.output_type
        self.operations = [k for k,v in all_operations.items() if v or all_false]

    def input_paths_resolution(self, paths: List[Path]):
        """Resolve all child paths of input paths"""

        all_paths = []
        for path in paths:
            # If input path isn't a file or a directory, ignore it
            if not path.is_file() and not path.is_dir():
                print(f"WARNING: Invalid path {path}")
                continue
            
            child_paths = self.recursive_path_traversal(path)
            all_paths.extend(child_paths)
        
        return all_paths

    def recursive_path_traversal(self, path: Path):
        """Recursively resolve all child paths of path
        according to recursive flag
        """

        if path.is_file():
            return [path.resolve()]
        
        # If path isn't a file or directory, ignore it
        if not path.is_dir():
            return []

        # If path is a directory:
        # And the recursive flag has been added, search all child paths
        #   recursively and add files encountered
        # Otherwise, only add files directly inside input directory
        all_sub_paths = []
        for child_path in path.glob("*"):
            if self._recursive:
                grandchild_paths = self.recursive_path_traversal(child_path)
                all_sub_paths.extend(grandchild_paths)
            elif child_path.is_file():
                return [child_path.resolve()]

        return all_sub_paths